﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClasses
{
    class VariableDeclaration
    {
        const string x = "a";
        static int a;
        static long b;
        static float c;
        static double d;
        static decimal Dec;
        //Comment
        static void Main(string[] args)
        {


            string kadhir123;
            /*Assignment Operator
             * variable = value
             */
            kadhir123 = args[0];

            //Implicit Converter
            b = long.MaxValue;
            c = float.MaxValue;
            d = 1234.56789;
            Dec = decimal.MaxValue;

            //Explicit Type Converter
            a = (int)b;
            c = (float)Dec;
            Dec = (decimal)d;
            a = (int)Dec;

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
            Console.WriteLine(Dec);
            Console.WriteLine(kadhir123);
            Console.ReadLine();
        }
    }
}
