﻿using System;

namespace Kathir
{
    class Program
    {
        //<Access Specifier> <Access Modifier> <Return Type> <Function Name>(<Parameters>){<Function Body>}
        public static void Main(string[] a)
        {
            //Console.Write("Kathir, welcome to C# class");
            //Formatter Output
            //Console.WriteLine("{0} welcome to {1} class.", "Kathir", "C#");
            //<Access Specifier> <Access Modifier> <Data Type> variable_name
            Console.Write("Enter the Student Name: ");
            string name = Console.ReadLine();
            Console.Write("Enter the Class Name: ");
            string className = Console.ReadLine();
            //Console.WriteLine("{0} welcome to {1} class.", name, className);
            //String Interapolation  
            Console.WriteLine($"{name} welcome to {className} class.");
            Console.ReadLine();
        }
    }
}
