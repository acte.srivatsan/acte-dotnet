﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class Delegates
    {
        delegate int NumberChanger(int n);
        public delegate bool StringComparer(string a, string b);

        public bool KadhirStringComparer(string a, string b)
        {
            return a.Equals(b);
        }

        static int num = 10;
        public static int AddNum(int p)
        {
            num += p;
            return num;
        }

        public static int MultNum(int q)
        {
            num *= q;
            return num;
        }

        public static int getNum()
        {
            return num;
        }

        public static void Main(string[] args)
        {
            //Delegates obj = new Delegates();
            //StringComparer stringComparer = new StringComparer(obj.KadhirStringComparer);
            //Console.Write("Enter the first string");
            //var strA = Console.ReadLine();
            //Console.Write("Enter the first string");
            //var strB = Console.ReadLine();
            //printData(stringComparer,strA,strB);

            Console.Write("Enter the Value:");
            int anju = int.Parse(Console.ReadLine());

            NumberChanger nc;
            NumberChanger nc1 = new NumberChanger(AddNum);
            NumberChanger nc2 = new NumberChanger(MultNum);

            nc = nc1 + nc2;

            Console.WriteLine(nc(anju));
            Console.ReadLine();
        }

        public static void printData(StringComparer comparer,string s1,string s2)
        {
            Console.WriteLine($"Result of string comparison: {comparer(s1, s2)}");
        }
    }
}
